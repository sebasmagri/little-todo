/*global define*/

define([
    'underscore',
    'backbone',
    'models/todo'
], function (_, Backbone, Todo) {
    'use strict';

    var TodoCollection = Backbone.Collection.extend({
        // Reference to this collection's model.
        model: Todo,

        // Server
        url: 'http://localhost:8888/api/v1/todo',

        // Sorting
        sortField: 'updated',
        sortDirection: 'desc',

        // Filter down the list of all todo items that are finished.
        done: function () {
            return this.filter(function (todo) {
                return todo.get('status');
            });
        },

        // Filter down the list to only todo items that are still not finished.
        pending: function () {
            return this.without.apply(this, this.done());
        },

        comparator: function (p, n) {

            // Handle the priority case
            var priorityWeights = {
                'low': 0,
                'normal': 1,
                'high': 2,
                'immediate': 3
            };

            if (this.sortField == 'priority') {
                p = priorityWeights[p.get(this.sortField)];
                n = priorityWeights[n.get(this.sortField)];
            } else {
                p = p.get(this.sortField);
                n = n.get(this.sortField);
            }

            if (this.sortDirection == 'asc') {
                return p > n;
            } else {
                return p < n;
            }
        }
    });
    
    return new TodoCollection();
});
