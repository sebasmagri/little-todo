/*global require, $, localStorage*/

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    },
    paths: {
        backbone: 'libs/backbone',
        jquery: 'libs/jquery',
        templates: 'html',
        text: 'libs/text',
        underscore: 'libs/lodash',
        moment: 'libs/moment'
    }
});

require(['backbone', 'views/app', 'routers/main'], function(Backbone, AppView, Router) {

    $.ajaxSetup({
        beforeSend: function(xhr) {
            var username, token;
            var ltID = localStorage.getItem('littleTodoIdentity');
            var ltSession = localStorage.getItem('littleTodoSession');
            if (ltID && ltSession) {
                username = decodeURIComponent(escape(window.atob(ltID)));
                xhr.setRequestHeader('X-Auth-User', username);
                xhr.setRequestHeader('X-Auth-Key', ltSession);
            }
        },
        statusCode: {
            401: function() {
                console.error('Not authorized. Go login now.');
                Backbone.history.location.replace('#/login');
            }
        }
    });

    new Router();
    Backbone.history.start();

    new AppView();
});
