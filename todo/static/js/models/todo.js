/*global define*/

define([
    'backbone',
    'moment'
], function (Backbone, moment) {
    'use strict';

    return Backbone.Model.extend({
        defaults: {
            title: ''
        },

        dateTimeFormat : "DD-MM-YYYY HH:mm",

        set: function(attrs, opts) {
            if (attrs) {
                if (attrs.due) {
                    attrs.due = moment(attrs.due, this.dateTimeFormat)
                        .utc()
                        .valueOf();
                } else {
                    delete attrs.due;
                }
            }
            opts.data = JSON.stringify(attrs);

            return Backbone.Model.prototype.set.call(this, attrs, opts);
        },

        validate: function(attrs, opts) {
            var validationErrors = [];

            if (!attrs.title) {
                validationErrors.push({
                    field: 'title',
                    error: 'Title is needed'
                });
            }
            if (attrs.due) {
                if (!attrs.due._isAMomentObject) {
                    if (typeof(attrs.due) == 'number') {
                        attrs.due = moment(attrs.due);
                    } else if (typeof(attrs.due) == 'string') {
                        attrs.due = moment(attrs.due, this.dateTimeFormat);
                    } else {
                        validationErrors.push({
                            field: 'due',
                            error: 'Due date could not be processed'
                        });
                    }
                }
                if (!attrs.due.isValid()) {
                    validationErrors.push({
                        field: 'due',
                        error: 'Due date is invalid'
                    });
                }
            }
            if (validationErrors.length > 0) {
                return validationErrors;
            }
        }
    });
});
