/*global define, $, _, localStorage*/

define([
    'backbone',
    'text!templates/login.html'
], function (Backbone, Template) {
    'use strict';

    return Backbone.View.extend({
        el: '.todo',
        template: _.template(Template),

        events: {
            'click #todo_login': 'login',
            'keypress .form-control': 'loginOrRegisterOnEnter',
            'click .todo-show-register-form': 'registerForm',
            'click #todo_register': 'register'
        },

        render: function () {
            this.$el.html(this.template());

            return this;
        },

        registerForm: function(evt) {
            evt.preventDefault();
            this.showRegisterForm();
        },

        showRegisterForm: function() {
            $('.todo-login-errors').fadeOut();
            $('.todo-login-form-buttons').hide();
            $('.todo-register-form').fadeIn();
            $('.todo-register-form-buttons').show();
            $('input[name=todo_login_mode]').val('register');            
        },

        showLoginForm: function() {
            $('.todo-login-errors').fadeOut();
            $('.todo-login-form-buttons').show();
            $('.todo-register-form').fadeOut();
            $('.todo-register-form-buttons').hide();
            $('input[name=todo_login_mode]').val('login');
        },

        loginOrRegisterOnEnter: function(evt) {
            if (evt.keyCode != 13) return;
            if ($('input[name=todo_login_mode]').val() == 'register') {
                this.register(evt);
            } else {
                this.login(evt);
            }
        },

        register: function() {
            var self = this;
            var username = this.$el.find('input[name=username]').val();
            var email = this.$el.find('input[name=email]').val();
            var pwd = this.$el.find('input[name=password]').val();
            var pwd_confirm = this.$el.find('input[name=password_confirm]').val();
            if (pwd == pwd_confirm) {
                $('.todo-login-errors').fadeOut();
                $.ajax({
                    url: 'http://localhost:8888/api/v1/users',
                    contentType: 'application/json',
                    dataType: 'json',
                    method: 'POST',
                    data: JSON.stringify({
                        username: username,
                        email: email,
                        password: pwd
                    })
                }).done(function(data) {
                    self.showSuccessMessage(
                        'Registration Successful',
                        'You were successfully registrated. You can login now.'
                    );
                    self.showLoginForm();
                }).fail(function() {
                    self.showError('Registration failed', 
                                   'An error ocurred during registration. Please try again.');
                });
            } else {
                this.showError('Registration failed', 'Your passwords did not match');
            }
        },

        showSuccessMessage: function(title, text) {
            var $todoLoginSuccess = $('.todo-login-success');
            $todoLoginSuccess.hide();
            $todoLoginSuccess.find('h4').text(title);
            $todoLoginSuccess.find('p').html(text);
            $todoLoginSuccess.fadeIn();
        },

        showError: function(title, text) {
            var $todoLoginErrors = $('.todo-login-errors');
            $todoLoginErrors.hide();
            $todoLoginErrors.find('h4').text(title);
            $todoLoginErrors.find('p').html(text);
            $todoLoginErrors.fadeIn();
        },

        login: function() {
            var self = this;
            var username = this.$el.find('input[name=username]').val();
            var pwd = this.$el.find('input[name=password]').val();
            $.ajax({
                dataType: 'json',
                headers: {
                    'X-Auth-User': username,
                    'X-Auth-Key': pwd
                },
                method: 'GET',
                url: 'http://localhost:8888/api/v1/auth'
            }).done(function(authData) {
                localStorage.setItem(
                    'littleTodoIdentity',
                    window.btoa(unescape(encodeURIComponent(username)))
                );
                localStorage.setItem('littleTodoSession', authData['token']);
                Backbone.history.location.replace('#/');
            }).fail(function(xhr, status) {
                self.showError(
                    'Login Failed',
                    'Please try again or ' +
                        '<a href="#" class="todo-show-register-form">Register</a>'
                );
            });
        }
    });
});
