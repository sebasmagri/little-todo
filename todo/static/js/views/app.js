/*global define, $, _*/

define([
    'backbone'
], function (Backbone) {
    'use strict';

    return Backbone.View.extend({
        initialize: function () {
            console.info('Booting up app...');
        }

    });
});
