/*global define, $, _, localStorage*/

define([
    'backbone',
    'text!templates/todo_list.html',
    'collections/todos',
    'views/todo',
    'views/todo_form'
], function (Backbone, Template, TodoCollection, TodoView, TodoFormView) {
    'use strict';

    return Backbone.View.extend({
        el: '.todo',
        template: _.template(Template),

        // The DOM events specific to an item.
        events: {
            'click #todo_sort_due': 'sortDue',
            'click #todo_sort_priority': 'sortPriority',
            'click #todo_logout': 'logout'
        },

        initialize: function () {
            this.listenTo(TodoCollection, 'add', this.add);
            this.listenTo(TodoCollection, 'reset', this.reload);
            this.listenTo(TodoCollection, 'sync', this.reload);
            this.listenTo(TodoCollection, 'sort', this.reload);
            this.listenTo(TodoCollection, 'invalid', this.showValidationErrors);
            this.listenTo(TodoCollection, 'error', this.error);

            TodoCollection.fetch();
        },

        // Let's show the login page on error
        error: function(evt) {
            this.logout(evt);
        },

        add: function (item) {
            if (item.isValid()) {
                $('.todo-error-block').hide();
                var view = new TodoView({model: item});
                this.$list.append(view.render().el);
            }
        },

        render: function () {
            this.$el.html(this.template());

            this.$form = $('#todo_main_form');
            this.$list = $('#todo_list');

            // Instantiate main form with a default model
            this.$form.html(new TodoFormView().render().el);

            return this;
        },

        reload: function () {
            this.$form.html(new TodoFormView().render().el);
            this.$list.html('');
            TodoCollection.each(this.add, this);
        },

        showValidationErrors: function(model) {
            if (model.validationError) {
                $('.todo-validation-errors-list').html('');
                _.each(model.validationError, function(val, idx) {
                    $('.todo-validation-errors-list')
                        .append('<li>' + val.error + '</li>');
                });
                $('.todo-validation-errors').show();
            }
        },

        _sort: function(target, field) {
            var direction;
            var $directionWidget = $(target).find('.todo-sort-direction');

            $('.todo-sort-direction').removeClass('caret');
            $directionWidget.addClass('caret');

            direction = $directionWidget.data('sortDirection');
            direction = direction == 'desc' ? 'asc' : 'desc';
            $directionWidget.data('sortDirection', direction);

            if (direction == 'asc') {
                $directionWidget.css('transform', 'rotate(180deg)');
            } else {
                $directionWidget.css('transform', 'rotate(0deg)');
            }

            TodoCollection.sortField = field;
            TodoCollection.sortDirection = direction;
            TodoCollection.sort();
        },

        sortDue: function(evt) {
            evt.stopPropagation();
            this._sort(evt.currentTarget, 'due');
        },

        sortPriority: function(evt) {
            evt.stopPropagation();
            this._sort(evt.currentTarget, 'priority');
        },

        logout: function(evt) {
            localStorage.removeItem('littleTodoIdentity');
            localStorage.removeItem('littleTodoSession');
            Backbone.history.location.replace('#/login');
        }

    });
});
