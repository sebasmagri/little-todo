/*global define, $, _*/

define([
    'backbone',
    'moment',
    'text!templates/todo.html',
    'views/todo_form'
], function (Backbone, moment, Template, TodoFormView) {
    'use strict';

    return Backbone.View.extend({
        template: _.template(Template),

        // The DOM events specific to an item.
        events: {
            'click .complete': 'complete',
            'click .reopen': 'reopen',
            'click .update': 'update',
            'click .destroy':	'destroy'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'destroy', this.remove);
        },

        priorityToStyleMap: {
            low: 'low',
            normal: 'info',
            high: 'warning',
            immediate: 'danger'
        },

        getPriorityStyle: function () {
            return this.priorityToStyleMap[this.model.get('priority')];
        },

        getDueStyle: function (due) {
            var dueDT, curDT, timeDiff;

            curDT = moment();
            curDT.utc();

            timeDiff = due - curDT;

            if (timeDiff < 24 * 60 * 60 * 1000) {
                return 'warning';
            } else if (timeDiff < 0 ) {
                return 'danger';
            } else {
                return 'info';
            }
        },

        render: function () {
            var options = {
                'priorityStyle': this.getPriorityStyle()
            };
            var due = this.model.get('due');
            due = moment(due);
            var html;
            _.extend(options, {
                dueStyle: this.getDueStyle(due),
                dueISO: due.toISOString(),
                dueIN: due.fromNow(),
                added: moment(this.model.get('added')).fromNow(),
                updated: moment(this.model.get('updated')).fromNow()
            });

            html = this.template({
                model: this.model.toJSON(),
                options: options
            });
            this.$el.html(html);

            return this;
        },

        complete: function () {
            this.model.save({
                'status': 1
            });
        },

        reopen: function () {
            this.model.save({
                'status': 0
            });
        },

        update: function () {
            this.$el.find('.todo-view').toggle();
            var form = this.$el.find('.todo-edit');
            var formSettings = {
                el: form,
                editMode: true,
                saveLabel: 'Update'
            };
            form.html(new TodoFormView(
                {
                    model: this.model
                },
                formSettings
            ).render().el);
        },

        destroy: function () {
            this.model.destroy();
        }
    });
});
