/*global define, $, _*/

define([
    'backbone',
    'moment',
    'collections/todos',
    'text!templates/todo_form.html'
], function (Backbone, moment, TodoCollection, Template) {
    return Backbone.View.extend({
        template: _.template(Template),

        events: {
            'click .todo-advanced-toggle': 'toggleAdvanced',
            'click .todo-commit': 'create',
            'click .todo-update': 'update',
            'click .todo-cancel-update': 'cancelUpdate'
        },

        defaultOptions: {
            autofocus: false,
            modelDefaults: {
                title: 'What do you want to do?',
                priority: 'normal'
            },
            priorities: ['low', 'normal', 'high', 'immediate'],
            saveLabel: 'Save',
            editMode: false
        },

        initialize: function (model, options) {
            if (!this.el) {
                this.el = options.el;
            }
            this.$el = $(this.el);
            this.options = {};
            _.extend(this.options, this.defaultOptions, options);
        },

        render: function (evt) {

            // Prepare
            var modelJSON;

            if (this.model) {
                modelJSON = this.model.toJSON();
                var due = this.model.get('due');
                if (due) {
                    modelJSON['due'] = moment(due).format('DD-MM-YYYY HH:mm');
                }
            } else {
                modelJSON = {};
            }

            var html = this.template({
                model: modelJSON,
                options: this.options
            });

            this.$el.html(html);

            return this;
        },

        toggleAdvanced: function (evt) {
            evt.preventDefault();
            $(evt.currentTarget).next().toggle();
        },

        create: function () {
            TodoCollection.create({
                title: this.$el.find('.todo-title').val().trim(),
                priority: this.$el.find('.todo-priority').val(),
                due: this.$el.find('.todo-due').val().trim()
            });
        },

        update: function () {
            this.model.save({
                title: this.$el.find('.todo-title').val().trim(),
                priority: this.$el.find('.todo-priority').val(),
                due: this.$el.find('.todo-due').val().trim()
            });
        },

        cancelUpdate: function() {
            this.model.trigger('change');
        }

    });
});
