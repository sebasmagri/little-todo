/*global define*/
define([
    'jquery',
    'backbone',
    'views/todo_list',
    'views/login'
], function ($, Backbone, TodoListView, LoginView) {
    'use strict';

    return Backbone.Router.extend({
        routes: {
            '': 'index',
            'login': 'login'
        },

        index: function () {
            console.log('index');
            new TodoListView().render();
        },

        login: function() {
            console.log('login');
            new LoginView().render();
        }
    });
});
