import importlib

from bson.objectid import ObjectId
from bson.json_util import dumps, loads
from pymongo import MongoClient

from jsonschema import validate, ValidationError


from .schemas import USER_SCHEMA, TODO_ITEM_SCHEMA
from .utils import oid_to_id


config = importlib.import_module('config')


class BaseStorage:
    def __init__(self):
        self.host = config.DB_HOST
        self.port = config.DB_PORT
        self.db_name = config.DB_NAME

    @property
    def col(self):
        return MongoClient(self.host, self.port)[self.db_name][self.collection]

    def validate(self, dict_):
        # It would be nice if validate returned a truey value
        # on successful validation
        validate(dict_, self.schema)
        # Update incoming object with default values
        for k, v in self.schema['properties'].items():
            if 'default' in v and k not in dict_:
                dict_[k] = v['default']
        return dict_

    def upsert(self, document):
        # Run the doc through schema validation
        valid_doc = self.validate(document)
        # Convert to BSON
        valid_doc = loads(dumps(valid_doc))

        # Return object id as a string
        return str(self.col.save(valid_doc))

    def remove(self, id_):
        self.col.remove(ObjectId(id_))

    @oid_to_id
    def get(self, query={}):
        return self.col.find(query)


class UserStorage(BaseStorage):
    collection = 'users'
    schema = USER_SCHEMA

    def upsert(self, user):
        if self.get({'username': user['username']}):
            raise ValidationError(
                'User with username %s already exists' % (user['username'])
            )
        return super(UserStorage, self).upsert(user)


class TodoStorage(BaseStorage):
    collection = 'items'
    schema = TODO_ITEM_SCHEMA
