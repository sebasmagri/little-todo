'''JSON-schemas for entities in the application
'''


USER_SCHEMA = {
    'description': 'An user of the application',
    'type': 'object',
    'properties': {
        'username': {
            'type': 'string',
            'maxLength': 64
        },
        'password': {
            'type': 'string',
            'description': 'The ultra secret password'
        },
        'roles': {
            'type': 'array'
        },
        'email': {
            'type': 'string',
            'description': 'The user\'s e-Mail address',
        },
        'joined': {
            'description': 'Timestamp of when the user joined',
            'type': 'integer'
        }
    },
    'required': [
        'username',
        'password'
    ]
}

TODO_ITEM_SCHEMA = {
    'description': 'A TODO item',
    'type': 'object',
    'properties': {
        'title': {
            'description': 'Title of the todo item',
            'type': 'string',
        },
        'status': {
            'description': 'Current status of the TODO item',
            'type': 'integer',
            'default': 0,
            'enum': [0, 1]
        },
        'priority': {
            'description': 'Priority of the TODO item',
            'type': 'string',
            'default': 'normal',
            'enum': ['low', 'normal', 'high', 'immediate']
        },
        'added': {
            'description': 'Timestamp in millis of the item addition in UTC',
            'type': 'integer'
        },
        'updated': {
            'description': 'Timestamp in millis of the last update in UTC',
            'type': 'integer'
        },
        'due': {
            'description': 'Timestamp of the due date for the item',
            'type': 'integer'
        },
        'tags': {
            'type': 'array'
        },
        'owner': {
            'type': 'string',
            'description': 'Username of the item owner'
        }
    },
    'required': [
        'title',
        'owner'
    ]
}
