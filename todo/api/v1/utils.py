'''Utility methods
'''
import json

from functools import wraps

from bson.json_util import dumps, loads


def oid_to_id(f):
    '''Decorator to translate query's JSON to Mongo Extended JSON and
    the db BSON data back to JSON
    '''
    @wraps(f)
    def wrap(inst, *args):
        query = loads(dumps(args[0]))
        ret = None
        try:
            ret = json.loads(dumps(list(f(inst, query))))
        except TypeError:
            ret = json.loads(dumps(f(inst, query)))
        finally:
            for doc in ret:
                oid = doc.pop('_id')
                doc['id'] = oid['$oid']
            return ret
    return wrap


def params_to_dict(params, schema):
    query = {
        k: v
        for k, v in params.items()
        if k in schema['properties']
    }
    if 'id' in params:
        _id = params.pop('id')
        query['_id'] = {
            '$oid': _id
        }
    return query
