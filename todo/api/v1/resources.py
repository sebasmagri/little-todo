'''API resources
'''
import json
import time

import falcon

from jsonschema import ValidationError

from .auth import encrypt_password, get_token
from .storage import TodoStorage, UserStorage
from .utils import params_to_dict


class BaseResource:
    location = '/'

    def __init__(self, config):
        self.config = config
        self.db = self.storage_class()


class BaseCollectionResource(BaseResource):

    def pre_save(self, request, response, document):
        return document

    def pre_get(self, request, response, query):
        return query

    def on_get(self, request, response):
        # To handle filters through url params
        query = params_to_dict(request._params, self.db.schema)

        try:
            query = self.pre_get(request, response, query)
            data = self.db.get(query)
        except Exception as e:
            raise falcon.HTTPInternalServerError(
                'Internal server error',
                str(e)
            )
        else:
            response.status = falcon.HTTP_200
            response.body = json.dumps(data)

    def on_post(self, request, response):
        doc_id = None
        try:
            stream = request.stream.read()
            post = json.loads(stream.decode('utf-8'))
            document = params_to_dict(post, self.db.schema)
            if document:
                # Call pre_save
                document = self.pre_save(request, response, document)
                doc_id = self.db.upsert(document)
            else:
                raise falcon.HTTPBadRequest('', '')
        except falcon.HTTPBadRequest as e:
            raise falcon.HTTPBadRequest(
                'Invalid request',
                'Could not extract any useful information'
            )
        except ValidationError as e:
            raise falcon.HTTPBadRequest(
                'Invalid request',
                str(e)
            )
        except Exception as e:
            raise falcon.HTTPInternalServerError(
                'Internal server error',
                str(e) if getattr(self.config, 'DEBUG', False) else ''
            )
        else:
            response.status = falcon.HTTP_200
            response.location = '%s/%s' % (self.location, doc_id)
            document['id'] = doc_id
            response.body = json.dumps(document)


class BaseItemResource(BaseResource):

    def pre_put(self, request, response, document):
        return document

    def on_get(self, request, response, id_):
        query = {
            '_id': {
                '$oid': id_
            }
        }
        try:
            data = self.db.get(query)[0]
        except IndexError:
            raise falcon.HTTPNotFound(
                'Item not found',
                ''
            )
        except Exception as e:
            raise falcon.HTTPInternalServerError(
                'Internal server error',
                str(e) if getattr(self.config, 'DEBUG', False) else ''

            )
        else:
            response.status = falcon.HTTP_200
            response.body = json.dumps(data)

    def on_delete(self, request, response, id_):
        try:
            self.db.remove(id_)
        except Exception as e:
            raise falcon.HTTPInternalServerError(
                'Internal server error',
                str(e) if getattr(self.config, 'DEBUG', False) else ''
            )
        else:
            response.status = falcon.HTTP_200

    def on_put(self, request, response, id_):
        try:
            stream = request.stream.read()
            payload = json.loads(stream.decode('utf-8'))
            payload['id'] = id_
            document = params_to_dict(payload, self.db.schema)
            if document:
                document = self.pre_put(request, response, document)
                self.db.upsert(document)
            else:
                raise falcon.HTTPBadRequest(
                    'Invalid request',
                    'Could not extract any useful information'
                )
        except Exception as e:
            raise falcon.HTTPInternalServerError(
                'Internal server error',
                str(e) if getattr(self.config, 'DEBUG', False) else ''
            )
        else:
            response.status = falcon.HTTP_200
            response.body = json.dumps(document)


class TodoItemsResource(BaseCollectionResource):
    storage_class = TodoStorage
    location = '/api/v1/todo'

    def pre_save(self, request, response, item):
        item['owner'] = request.env['HTTP_X_AUTH_USER']
        item['added'] = int(time.time() * 1000)
        return item

    def pre_get(self, request, response, query):
        query['owner'] = request.env['HTTP_X_AUTH_USER']
        return query


class TodoItemResource(BaseItemResource):
    storage_class = TodoStorage
    location = '/api/v1/todo/{id_}'

    def pre_put(self, request, response, item):
        item['updated'] = int(time.time() * 1000)
        return item


class UsersResource(BaseCollectionResource):
    location = '/api/v1/users'
    storage_class = UserStorage

    def pre_save(self, request, response, user):
        user['password'] = encrypt_password(user['password'])
        user['joined'] = int(time.time() * 1000)
        return user


class TokensResource(BaseResource):
    location = '/api/v1/auth'
    storage_class = UserStorage

    def on_get(self, request, response):
        identity = request.env['wsgi.identity']
        response.status = falcon.HTTP_200
        response.body = json.dumps({
            'token': get_token(identity.login)
        })
