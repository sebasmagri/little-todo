import importlib
import re

from talons.auth.middleware import Middleware

from itsdangerous import (
    TimedJSONWebSignatureSerializer as TokenSerializer,
    SignatureExpired,
    BadSignature
)
from passlib.hash import sha256_crypt

from .storage import TodoStorage, UserStorage

config = importlib.import_module('config')


USER_DB = UserStorage()
TODO_DB = TodoStorage()


# Auth Utils
def encrypt_password(pwd):
    return sha256_crypt.encrypt(pwd)


def get_token(username, expiration=600):
    user = USER_DB.get({'username': username})[0]
    user_id = user.get('id')
    s = TokenSerializer(config.SECRET_KEY, expires_in=expiration)
    return s.dumps({'id': user_id}).decode('utf-8')


# Authenticators
def authenticate_with_password(identity):
    # Get user info from db
    user = USER_DB.get({
        'username': identity.login,
    })
    if user and sha256_crypt.verify(identity.key,
                                    user[0].get('password')):
        identity.roles = user[0].get('roles', [])
        return True
    return False


def authenticate_with_token(identity):
    # Get user info from db
    user = USER_DB.get({
        'username': identity.login,
    })
    serializer = TokenSerializer(config.SECRET_KEY)
    try:
        token_data = serializer.loads(identity.key)
    except SignatureExpired:
        return False
    except BadSignature:
        return False
    else:
        if token_data['id'] == user[0].get('id'):
            identity.roles = user[0].get('roles', [])
            return user
        return False


# Authorizers
def anyone(*args):
    return True


def admin_or_owner(match, identity):
    _id = match.groups(1)[0]
    if 'admin' in identity.roles:
        return True
    # If not admin, let's check if the user owns the item
    item = TODO_DB.get({
        '_id': {
            '$oid': _id
        }
    })[0]
    if item.get('owner') == identity.login:
        return True
    return False

AUTHORIZATION_RULES = (
    # api.v1.auth.get
    (r'^api\.v1\.auth\.get$', anyone),  # for tokens
    # api.v1.users.post
    (r'^api\.v1\.users\.post$', anyone),  # for registrations
    # api.v1.resource.options
    (r'^api\.v1.*\.options$', anyone),
    # api.v1.todo.post
    (r'^api\.v1\.todo\.post$', anyone),
    # api.v1.todo.get
    (r'^api\.v1\.todo\.get$', anyone),
    # api.v1.todo.foobar.put
    (r'^api\.v1\.todo\.(.+)\.put$', admin_or_owner),
    # api.v1.todo.foobar.get
    (r'^api\.v1\.todo\.(.+)\.get$', admin_or_owner),
    # api.v1.todo.foobar.delete
    (r'^api\.v1\.todo\.(.+)\.delete$', admin_or_owner),
)

_AUTHORIZATION_RULES = [
    (re.compile(regex), fn)
    for regex, fn
    in AUTHORIZATION_RULES
]


def authorize(identity, resource_action):
    res_string = resource_action.to_string()
    for p, fn in _AUTHORIZATION_RULES:
        m = p.match(res_string)
        if m:
            return fn(m, identity)


# Custom Middleware class to allow unauthed OPTIONS
# and registrations
class TodoAuthMiddleware(Middleware):
    def __init__(self, identifiers, authenticators, **conf):
        super(TodoAuthMiddleware, self).__init__(
            identifiers,
            authenticators,
            **conf
        )

    def __call__(self, request, response, params):
        if request.method != 'OPTIONS' and \
           (request.method, request.path) != ('POST', '/api/v1/users'):
            super(TodoAuthMiddleware, self).__call__(request, response, params)
