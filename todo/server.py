'''A simple TODO webapp backend
'''
import falcon

from talons.auth.external import Authenticator, Authorizer
from talons.auth.httpheader import Identifier

import config

from api.v1.resources import (
    TodoItemsResource, TodoItemResource, TokensResource, UsersResource
)
from api.v1.auth import TodoAuthMiddleware


# Set up authentication middleware

# Identifiers
TODO_IDENTIFIER = Identifier(
    httpheader_user='X-Auth-User',
    httpheader_key='X-Auth-Key'
)

# Authenticators
TODO_TOKEN_AUTHENTICATOR = Authenticator(
    external_authfn='api.v1.auth.authenticate_with_token',
    external_sets_roles=True
)
TODO_PASSWORD_AUTHENTICATOR = Authenticator(
    external_authfn='api.v1.auth.authenticate_with_password',
    external_sets_roles=True
)

# Authorizers
TODO_AUTHORIZER = Authorizer(
    external_authzfn='api.v1.auth.authorize'
)

AUTH_MIDDLEWARE = TodoAuthMiddleware(
    identifiers=[TODO_IDENTIFIER, ],
    authenticators=[
        TODO_TOKEN_AUTHENTICATOR,
        TODO_PASSWORD_AUTHENTICATOR
    ],
    authorizer=TODO_AUTHORIZER
)


def cors_middleware(request, response, params):
    response.set_header(
        'Access-Control-Allow-Origin',
        'http://localhost:8000'
    )
    response.set_header(
        'Access-Control-Allow-Credentials',
        'true'
    )
    response.set_header(
        'Access-Control-Allow-Headers',
        'X-Auth-User, X-Auth-Key, Content-Type'
    )
    response.set_header(
        'Access-Control-Allow-Methods',
        'GET, PUT, POST, DELETE, OPTIONS'
    )


APP = falcon.API(before=[cors_middleware, AUTH_MIDDLEWARE])

RESOURCES = (
    TodoItemsResource,
    TodoItemResource,
    TokensResource,
    UsersResource
)


for res in RESOURCES:
    APP.add_route(res.location, res(config))

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    server = make_server('127.0.0.1', 8888, APP)
    server.serve_forever()
