=========================
 Simple TODO application
=========================

Setup
=====

You'll need access to a MongoDB server. You can set DB connection details in `todo/config.py`.

You must also set a SECRET_KEY in `todo/config.py`, you can generate one issuing::

    $ python -c 'import os; print(os.urandom(32))'

Dependencies
============

You should run this application using Python 3.

To install the dependencies you should use a virtual environment::

    $ virtualenv env
    $ source env/bin/activate
    $ pip install -r requirements/production.txt

Running
=======

To run the backend you can do::

    $ cd todo
    $ gunicorn server:APP

The frontend is at `todo/static` and should be published with a webserver.
You can run a development webserver with::

    $ cd todo/static
    $ python -m http.server